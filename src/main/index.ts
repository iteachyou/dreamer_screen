import {app, BrowserWindow, desktopCapturer, ipcMain, dialog, shell, screen, clipboard} from 'electron'
import {electronApp, optimizer} from '@electron-toolkit/utils'
import MainFrame from "./frames/MainFrame";
import EventRouter from "./router/EventRouter";
import routers from "./router/router.template";

function createWindow(): void {
  const eventRouter = new EventRouter()
  // 创建主窗体
  const main : MainFrame = new MainFrame();

  eventRouter.addApi('app', app)
  eventRouter.addApi('window', main)
  eventRouter.addApi('dialog', dialog)
  eventRouter.addApi('shell', shell)
  eventRouter.addApi('clipboard', clipboard)
  eventRouter.addApi('process', process)
  eventRouter.addApi('screen', screen)
  eventRouter.addApi('capturer', desktopCapturer)
  eventRouter.addRoutes(routers)

  ipcMain.handle('renderer-to-main', (event, data) => {
    eventRouter.router(data)
  })

  // HMR for renderer base on electron-vite cli.
  // Load the remote URL for development or the local html file for production.
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  app.commandLine.appendSwitch('ignore-certificate-errors')
  // Set app user model id for windows
  electronApp.setAppUserModelId('cc.iteachyou.dreamer.screen')

  // Default open or close DevTools by F12 in development
  // and ignore CommandOrControl + R in production.
  // see https://github.com/alex8088/electron-toolkit/tree/master/packages/utils
  app.on('browser-window-created', (_, window) => {
    optimizer.watchWindowShortcuts(window)
  })

  createWindow()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
