import { BrowserWindow, WebContents } from "electron";
import { is } from "@electron-toolkit/utils";
import path from "path";

export default class MainFrame {
  #window: BrowserWindow = null;
  #webContents: WebContents = null;

  constructor() {
    this.#window = new BrowserWindow({
      width: 850,
      height: 330,
      frame: false,
      show: false,
      backgroundColor: '#00000000',
      resizable: false,
      transparent: true,
      autoHideMenuBar: true,
      ...(process.platform === 'linux'
        ? {
          icon: path.join(__dirname, '../../build/icon.png')
        }
        : {}),
      webPreferences: {
        preload: path.join(__dirname, '../preload/index.js'),
        sandbox: false
      }
    })

    this.#window.on('ready-to-show', () => {
      this.#window.show()
      this.#webContents = this.#window.webContents
    })

    if (is.dev && process.env['ELECTRON_RENDERER_URL']) {
      //this.#window.webContents.openDevTools()
      this.#window.loadURL(process.env['ELECTRON_RENDERER_URL'])
    } else {
      this.#window.loadFile(path.join(__dirname, '../renderer/index.html'))
    }
  }

  getWindow(){
    return this.#window
  }
  getWebContents(){
    return this.#webContents
  }
}
