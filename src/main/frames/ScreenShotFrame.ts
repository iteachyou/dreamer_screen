import {BrowserWindow, WebContents, screen, ipcMain, desktopCapturer} from "electron";
import path from "path";
import {is} from "@electron-toolkit/utils";

export default class ScreenShotFrame {
  #window: BrowserWindow = null
  #webContents: WebContents = null

  constructor() {
    screen.getPrimaryDisplay()
    const {width, height} = screen.getPrimaryDisplay().size

    this.#window = new BrowserWindow({
      width: width,
      height: height,
      frame: false,
      show: false,
      alwaysOnTop: true,
      resizable: false,
      transparent: true,
      autoHideMenuBar: true,
      ...(process.platform === 'linux'
        ? {
          icon: path.join(__dirname, '../../build/icon.png')
        }
        : {}),
      webPreferences: {
        preload: path.join(__dirname, '../preload/index.js'),
        sandbox: false
      }
    })

    this.#window.on('ready-to-show', () => {
      this.#webContents = this.#window.webContents
    })

    this.#window.on('closed', () => {
      this.#window = null
      this.#webContents = null
    })

    if (is.dev && process.env['ELECTRON_RENDERER_URL']) {
      //this.#window.webContents.openDevTools()
      this.#window.loadURL(process.env['ELECTRON_RENDERER_URL'] + "/#/screenshot")
    } else {
      this.#window.loadFile(path.join(__dirname, '../renderer/index.html'), {
        hash: '/screenshot'
      })
    }
  }

  invoke(data){
    this.#webContents.send('main-to-renderer', {
      name: 'window-resized',
      event: 'event',
      data: data
    })
  }

  getWindow(){
    return this.#window
  }

  getWebContents(){
    return this.#webContents
  }

  show(){
    this.#window.show()
  }
}
