/**
 * 任意数字准换成 时分秒
 * @param time
 * @return
 */
export function number2date(times){
  if(typeof times ==='number'){
    if(times <= 0){
      return '00:00:00';
    }else{
      let hh = parseInt(times / 3600); //小时
      let shh = times - hh * 3600;
      let ii = parseInt(shh / 60);
      let ss = shh - ii * 60;
      return (hh < 10 ? '0'+hh : hh) + ':' + (ii < 10 ? '0'+ii : ii) +':'+(ss < 10 ? '0'+ss : ss);
    }
  }else{
    return '00:00:00';
  }
}
