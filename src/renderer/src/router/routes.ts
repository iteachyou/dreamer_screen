//系统路由
const routes = [
  {
    path: "/",
    component: () => import('@renderer/views/main/index.vue'),
    meta: {
      title: "首页"
    }
  },
  {
    path: "/region",
    component: () => import('@renderer/views/region/index.vue'),
    meta: {
      title: "选择区域"
    }
  },
  {
    path: "/screenshot",
    component: () => import('@renderer/views/screenshot/index.vue'),
    meta: {
      title: "屏幕截图"
    }
  }
]

export default routes;
