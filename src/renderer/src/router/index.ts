import { createRouter, createWebHashHistory } from 'vue-router'
import routes from './routes';

var router = createRouter({
  history: createWebHashHistory(),
  routes: routes
})

router.beforeEach(async (to, from, next) => {
  document.title = to.meta.title
  next()
})

router.afterEach((to, from) => {

})

export default router
